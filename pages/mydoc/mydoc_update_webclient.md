---
title: Update Psono Webclient
summary: "Update guide of the Psono Admin Webclient"
tags: [installation, client]
sidebar: mydoc_sidebar
permalink: mydoc_update_webclient.html
folder: mydoc
---

## Update with Docker

1.  Update the docker image

	    docker pull psono/psono-client:latest

2.  Stop old psono-client

        docker stop psono-client

3.  Start new psono-client

        docker run --name psono-client-new \
            -v /opt/docker/psono-client/config.json:/usr/share/nginx/html/config.json \
            -v /opt/docker/psono-client/privacy-policy-content.html:/usr/share/nginx/html/privacy-policy-content.html \
            -d --restart=unless-stopped -p 10101:80 psono/psono-client:latest
            
    
    {% include note.html content="Leave out the line with the privacy policy if you have no own" %}

4.  Cleanup

	If everything works you can cleanup your containers with the following commands:

		docker rm psono-client
		docker rename psono-client-new psono-client

If anything fails you should be able to start the  old docker container again.

## Update without Docker

The webclient is a pure html / js website, that can be hosted with any webserver and has zero dependencies.

1.  Download the webclient artifact

	Visit the following url and download the webclient:

	[psono.jfrog.io/psono/psono/client/latest/webclient.zip](https://psono.jfrog.io/psono/psono/client/latest/webclient.zip)

2.  Backup htdocs folder

	Before you replace any files you should backup your htdocs folder

2.  Install webclient

    Unpack the webclient into the htdocs folder of your webserver.

3.  Restore `config.json`

	Copy the old `config.json` and replace the one in the htdocs folder

4.  (optional) Restore `privacy-policy-content.html`

	Copy the old `privacy-policy-content.html` and replace the one in the htdocs folder

If anything fails you should be able to restore your webclient restoring the files from the backuped folder.

{% include links.html %}
